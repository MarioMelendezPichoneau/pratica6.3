﻿using System;

namespace practica6._3
{
    // Crear tres clases ClaseA, ClaseB, ClaseC que ClaseB herede de ClaseA y ClaseC herede de ClaseB. 
    // Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de la clase ClaseC.

    class ClaseA
    {
        public void Papa()
        {
            Console.WriteLine("Soy Multi Millonario");
        }
    }

    class ClaseB : ClaseA
    {
        public void hijo()
        {
            Console.WriteLine("Dueño de multiples empresas ");
        }
    }
    class ClaseC : ClaseB
    {
        public void Nieto()
        {
            Console.WriteLine("Estudiante universitario");

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ClaseC ana = new ClaseC();
            ana.Papa();
            ana.hijo();
            ana.Nieto();

        }
    }
   
}
